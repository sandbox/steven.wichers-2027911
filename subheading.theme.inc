<?php
/**
 * @file
 * Theme functions for dealing with subheadings.
 */

/**
 * Returns HTML for a subheading.
 *
 * Provides a simple themeing mechanism for consistently themeing subheadings.
 *
 * @param array $variables
 *   An associative array containing the keys 'html_tag', 'subheading', and
 *   'attributes'.
 *
 * @return string
 *   A rendered subheading.
 */
function theme_subheading($variables) {

  $tag = isset($variables['html_tag']) ? $variables['html_tag'] : 'p';
  if (!empty($tag)) {

    $attributes = array();

    if (!empty($variables['attributes'])) {

      $attributes = array_merge_recursive($attributes, $variables['attributes']);
    }

    $output = array(
      '#theme' => 'html_tag',
      '#tag' => $tag,
      '#value' => $variables['subheading'],
      '#attributes' => $attributes,
    );

    return drupal_render($output);
  }

  return $variables['subheading'];
}

/**
 * Returns HTML for a subheading formatted field.
 *
 * This mainly serves as a helper theme function to change parameters to match
 * those of theme_subheading() as well as plain-texting the subheading when
 * configured.
 *
 * @param array $variables
 *   An associative array as provided by subheading_field_formatter_view().
 *
 * @return string
 *   A rendered subheading.
 */
function theme_subheading_formatter_subheading_default($variables) {

  $settings = $variables['settings'];

  $subheading = $variables['element'];
  $attributes = array();

  if (!empty($settings['plain'])) {

    $subheading = strip_tags($subheading);
    $subheading = check_plain($subheading);
  }

  if (!empty($settings['wrapper class'])) {

    $attributes['class'] = array($settings['wrapper class']);
  }

  return theme('subheading', array(
    'subheading' => $subheading,
    'html_tag' => $settings['wrapper'],
    'attributes' => $attributes,
  ));
}
