INTRODUCTION
------------

Provides a consistent interface for dealing with subtitles. Instead of having
to override the way a field looks with a tpl, configuring DS per field, etc. a
simple theme function can be called or a field formatter can be used.


INSTALLATION
------------

 * Install as usual, see http://drupal.org/node/70151 for further information.

CONFIGURATION
-------------

CONTACT
-------

Current maintainers:
* Steven Wichers (steven.wichers) - http://drupal.org/user/1774136

This project has been sponsored by:
* McMurry/TMG
  McMurry/TMG is a world-leading, results-focused content marketing firm. We
  leverage the power of world-class content — in the form of the broad
  categories of video, websites, print and mobile — to keep our clients’ brands
  top of mind with their customers.  Visit http://www.mcmurrytmg.com for more
  information.
